<?php

include_once(__DIR__ . '/../src/config.php');
include_once(__DIR__ . '/../src/functions.php');
include_once(__DIR__ . '/../src/router.php');
include_once(__DIR__ . '\../src/pages\home.php');
include_once(__DIR__ . '\../src/pages\student.php');


$dbConnection = getDbConnection();

$objTest = new ObjTest($routes, $dbConnection);
$objTest->init($routes, $dbConnection);