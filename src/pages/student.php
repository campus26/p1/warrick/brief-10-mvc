<?php
class StudentController
{
    function __construct(){
        include_once(__DIR__ . '/../model/StudentModel.php');
        $this->studentmodel = new StudentModel();
    }

    public function index($dbConnection)
    {
        $this->studentmodel->index($dbConnection);
    }

    public function view($dbConnection)
    {
        $studentId = getParam('studentId');
        $student = $this->findById($dbConnection, $studentId);
        $pageName = 'Students';
        $panelHeader = 'Student Information';

        include_once(TEMPLATES . 'student/view.php');
    }

    public function findById($dbConnection, $studentId)
    {
        $this->studentmodel->findById($dbConnection, $studentId);
    }

    public function add($dbConnection)
    {
        $this->studentmodel->add($dbConnection);
    }

    public function edit($dbConnection)
    {
        $studentId = getParam('studentId');
        $student = $this->findById($dbConnection, $studentId);

        echo json_encode($student);
    }

    public function update($dbConnection)
    {
        $this->studentmodel->update($dbConnection);
    }

    public function delete($dbConnection)
    {
        $this->studentmodel->delete($dbConnection);
    }
}
