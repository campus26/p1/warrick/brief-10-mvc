<?php
define('HOME', 'home/');


class HomeController
{

    public function index()
    {
        $pageName = 'Home';

        include_once(TEMPLATES . HOME . 'index.php');
    }
}
